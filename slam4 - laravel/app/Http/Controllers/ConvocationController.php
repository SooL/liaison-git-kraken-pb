<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Convocation;

class ConvocationController extends Controller
{
    public function listAll(){
      $tab=Convocation::all();
    return view('lesConvocations') -> with('tab', $tab);
    }

    public function create(){
      return view ("create");
    }
    public function postCreate(Request $request){
      $c = new Convocation;
      $c -> nom=$request -> get('nom');
      $c -> motif=$request -> get('motif');
      $c -> save();

      return redirect('lesConvocations');
    }

    public function destroy(Convocation $c){
      $c->delete();
    }
}
