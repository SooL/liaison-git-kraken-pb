@extends("template")
@section("title")
Convocations
@stop

@section("content")

<table>
  <thead>
    <td>Nom</td>
    <td>Motif</td>
    <td>Action</td>
  </thead>
  <tbody>
    <a href="{{route('Creer')}}"><button>Creer</button></a>
  @foreach ($tab as $ligne)
    <tr>
      <td>{{$ligne["nom"]}}</td>
      <td>{{$ligne["motif"]}}</td>
      <td>
        <a href="{{route('Supprimer')}}"><button>Supprimer<button></a>
      </td>
    </tr>
  @endforeach
  </tbody>
</table>
@stop
