<?php

use Illuminate\Database\Seeder;

class ConvocationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('convocations')->insert([
        'nom'=>'TestNom',
        'motif'=>'TestMotif',
      ]);

        DB::table('convocations')->insert([
          'nom'=>'TestNom2',
          'motif'=>'TestMotif2',
        ]);

          DB::table('convocations')->insert([
            'nom'=>'TestNom3',
            'motif'=>'TestMotif3',
          ]);
    }
}
