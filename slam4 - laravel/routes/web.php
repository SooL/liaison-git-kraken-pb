<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('Acceuil', function () {
  return view('Acceuil');
}) -> name ("Acceuil");


Route::get('Reglement', function () {
  return view('Reglement');
}) -> name("Reglement");

Route::get('lesConvocations', "ConvocationController@listAll") -> name("lesConvocations");

Route::get('create', 'ConvocationController@create') -> name("Creer");
Route::post('postCreate', 'ConvocationController@postCreate') -> name("CreerPost");

Route::delete('destroy', 'ConvocationController@destroy') -> name('Supprimer');
